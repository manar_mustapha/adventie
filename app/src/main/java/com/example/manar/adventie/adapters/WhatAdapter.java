package com.example.manar.adventie.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.manar.adventie.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by manar on 17/10/17.
 */

public class WhatAdapter extends RecyclerView.Adapter<WhatAdapter.DontMissViewHolder> {

    List<String> images = new ArrayList<>();
    List<String> texts = new ArrayList<>();
    Context context;

    public WhatAdapter(Context context) {

        this.context = context;
        images.add("av_icn_american");
        texts.add("american");
        images.add("av_icn_bakery");
        texts.add("Bakery");
        images.add("av_icn_cafe");
        texts.add("Cafe");
        images.add("av_icn_chinese");
        texts.add("chinese");
        images.add("av_icn_indian");
        texts.add("Food truck");
        images.add("av_icn_indian");
        texts.add("indian");
        images.add("av_icn_italian");
        texts.add("italian");
        images.add("av_icn_sea_food");
        texts.add("sea food");
        images.add("av_icn_tukish");
        texts.add("tukish");

    }

    @Override
    public DontMissViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.what, parent, false);
        return new DontMissViewHolder(view);
    }

    @Override
    public void onBindViewHolder(DontMissViewHolder holder, int position) {

        holder.setImage(images.get(position));
        holder.setText(texts.get(position));
    }

    @Override
    public int getItemCount() {
        return images.size();
    }

    class DontMissViewHolder extends RecyclerView.ViewHolder {

        ImageView imageView;
        TextView textView;

        DontMissViewHolder(View itemView) {

            super(itemView);
            imageView = itemView.findViewById(R.id.what_image);
            textView = itemView.findViewById(R.id.what_text);
        }

        void setImage(String imageResource) {
            imageView.setImageResource(context.getResources().getIdentifier(imageResource, "drawable", context.getPackageName()));
        }
        void setText(String text) {
            textView.setText(text);
        }
    }
}