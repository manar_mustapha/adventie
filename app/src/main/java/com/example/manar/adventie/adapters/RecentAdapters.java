package com.example.manar.adventie.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.manar.adventie.R;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by manar on 17/10/17.
 */

public class RecentAdapters extends RecyclerView.Adapter<RecentAdapters.BestViewHolder> {

    List<String> images = new ArrayList<>();
    Context context;

    public RecentAdapters(Context context) {

        this.context = context;
        images.add("harriet_welsch");
        images.add("harriet_welsch");
        images.add("harriet_welsch");
        images.add("harriet_welsch");
        images.add("harriet_welsch");
        images.add("harriet_welsch");
        images.add("harriet_welsch");
        images.add("harriet_welsch");
        images.add("harriet_welsch");
        images.add("harriet_welsch");
        images.add("harriet_welsch");
        images.add("harriet_welsch");
    }

    @Override
    public RecentAdapters.BestViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recent, parent, false);
        return new RecentAdapters.BestViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecentAdapters.BestViewHolder holder, int position) {

        holder.setImage(images.get(position));
        holder.setText("Arabyyah");
    }

    @Override
    public int getItemCount() {
        return images.size();
    }

     class BestViewHolder extends RecyclerView.ViewHolder {

         CircleImageView imageView;
         TextView textView;

        BestViewHolder(View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.profile);
            textView = itemView.findViewById(R.id.name);
        }

        void setImage(String imageResource) {
            imageView.setImageResource(context.getResources().getIdentifier(imageResource, "drawable", context.getPackageName()));
        }
        void setText(String textTitle){
            textView.setText(textTitle);
        }
    }
}