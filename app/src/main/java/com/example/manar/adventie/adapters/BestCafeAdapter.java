package com.example.manar.adventie.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.manar.adventie.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by manar on 17/10/17.
 */

public class BestCafeAdapter extends RecyclerView.Adapter<BestCafeAdapter.BestViewHolder> {

    List<String> images = new ArrayList<>();
    Context context;

    public BestCafeAdapter(Context context) {

        this.context = context;
        images.add("group_3_copy");
        images.add("group_3_copy");
        images.add("group_3_copy");
        images.add("group_3_copy");
        images.add("group_3_copy");
        images.add("group_3_copy");
        images.add("featured_by_gray");
        images.add("group_3_copy");
        images.add("group_3_copy");
        images.add("group_3_copy");
        images.add("group_3_copy");
        images.add("group_3_copy");
    }

    @Override
    public BestCafeAdapter.BestViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.best_item, parent, false);
        return new BestCafeAdapter.BestViewHolder(view);
    }

    @Override
    public void onBindViewHolder(BestCafeAdapter.BestViewHolder holder, int position) {

        holder.setImage(images.get(position));
        holder.setText("Arabyyah");
    }

    @Override
    public int getItemCount() {
        return images.size();
    }

     class BestViewHolder extends RecyclerView.ViewHolder {

         ImageView imageView;
         TextView textView;

        BestViewHolder(View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.best_item_image);
            textView = itemView.findViewById(R.id.best_title);
        }

        void setImage(String imageResource) {
            imageView.setImageResource(context.getResources().getIdentifier(imageResource, "drawable", context.getPackageName()));
        }
        void setText(String textTitle){
            textView.setText(textTitle);
        }
    }
}