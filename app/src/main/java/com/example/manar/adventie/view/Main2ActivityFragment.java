package com.example.manar.adventie.view;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.manar.adventie.R;
import com.example.manar.adventie.adapters.BestCafeAdapter;
import com.example.manar.adventie.adapters.DontMissAdapter;
import com.example.manar.adventie.adapters.FeaturedAdapter;
import com.example.manar.adventie.adapters.RecentAdapters;
import com.example.manar.adventie.adapters.WhatAdapter;

/**
 * A placeholder fragment containing a simple view.
 */
public class Main2ActivityFragment extends Fragment implements View.OnClickListener{

    public Main2ActivityFragment() {
    }

    RecyclerView dontMissRecyclerView ;
    DontMissAdapter dontMissAdapter;
    RecyclerView bestRecycleView ;
    BestCafeAdapter bestCafeAdapter;
    RecyclerView featuredRecycleView;
    FeaturedAdapter featuredAdapter;
    RecyclerView recentRecycleView;
    RecentAdapters recentAdapters;
    RecyclerView whatRecycleView;
    WhatAdapter whatAdapter;
    TextView review;
    TextView browseAll;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main2, container, false);

        dontMissRecyclerView = view.findViewById(R.id.dont_miss_recycle_view);
        dontMissRecyclerView.setHasFixedSize(true);
        dontMissRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL , false));
        //-----------------------------------------------------------------------------------------------------------------------
        bestRecycleView = view.findViewById(R.id.best_recycle_view);
        bestRecycleView.setHasFixedSize(true);
        bestRecycleView.setLayoutManager(new LinearLayoutManager(getActivity() , LinearLayoutManager.HORIZONTAL , false));
        //-----------------------------------------------------------------------------------------------------------------------
        featuredRecycleView = view.findViewById(R.id.featured_recycle_view);
        featuredRecycleView.setHasFixedSize(true);
        featuredRecycleView.setLayoutManager(new LinearLayoutManager(getActivity() , LinearLayoutManager.HORIZONTAL , false));
        //-----------------------------------------------------------------------------------------------------------------------
        recentRecycleView = view.findViewById(R.id.recent_views);
        recentRecycleView.setHasFixedSize(true);
        recentRecycleView.setLayoutManager(new LinearLayoutManager(getActivity() , LinearLayoutManager.HORIZONTAL , false));
        //-----------------------------------------------------------------------------------------------------------------------
        whatRecycleView = view.findViewById(R.id.looking_recycle_view);
        whatRecycleView.setHasFixedSize(true);
        whatRecycleView.setLayoutManager(new GridLayoutManager(getActivity() , 3));
        whatRecycleView.setNestedScrollingEnabled(false);

        review = view.findViewById(R.id.review);
        browseAll = view.findViewById(R.id.browse_all);
        browseAll.setOnClickListener(this);
        review.setOnClickListener(this);
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();

        dontMissAdapter = new DontMissAdapter(getActivity());
        dontMissRecyclerView.setAdapter(dontMissAdapter);
        //-----------------------------------------------------------------------------------------------------------------------
        bestCafeAdapter = new BestCafeAdapter(getActivity());
        bestRecycleView.setAdapter(bestCafeAdapter);
        //-----------------------------------------------------------------------------------------------------------------------
        featuredAdapter = new FeaturedAdapter(getActivity());
        featuredRecycleView.setAdapter(featuredAdapter);
        //-----------------------------------------------------------------------------------------------------------------------
        recentAdapters = new RecentAdapters(getActivity());
        recentRecycleView.setAdapter(recentAdapters);
        //-----------------------------------------------------------------------------------------------------------------------
        whatAdapter = new WhatAdapter(getActivity());
        whatRecycleView.setAdapter(whatAdapter);

    }

    @Override
    public void onClick(View view) {

        switch(view.getId()){
            case R.id.review:{
                startActivity(new Intent(getActivity() , TabActivity.class));
                break;
            }
            case R.id.browse_all:{
                startActivity(new Intent(getActivity() , BrowseActivity.class));
                break;
            }
        }

    }
}
