package com.example.manar.adventie.adapters;

/**
 * Created by manar on 20/10/17.
 */

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.manar.adventie.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by manar on 17/10/17.
 */

public class BrowseAllAdapter extends RecyclerView.Adapter<BrowseAllAdapter.BrowseAllHolder> {

    List<String> images = new ArrayList<>();
    Context context;

    public BrowseAllAdapter(Context context) {

        this.context = context;
        images.add("group_3_copy");
        images.add("group_3_copy");
        images.add("group_3_copy");
        images.add("group_3_copy");
        images.add("group_3_copy");
        images.add("group_3_copy");
        images.add("group_3_copy");
        images.add("group_3_copy");
        images.add("group_3_copy");
        images.add("group_3_copy");
        images.add("group_3_copy");
        images.add("group_3_copy");
    }

    @Override
    public BrowseAllHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.browse_all, parent, false);
        return new BrowseAllHolder(view);
    }

    @Override
    public void onBindViewHolder(BrowseAllHolder holder, int position) {

//        holder.setImage(images.get(position));
//        holder.setText("Arabyyah");
    }

    @Override
    public int getItemCount() {
        return images.size();
    }

    class BrowseAllHolder extends RecyclerView.ViewHolder {

//        ImageView imageView;
//        TextView textView;

        BrowseAllHolder(View itemView) {
            super(itemView);
//            imageView = itemView.findViewById(R.id.best_item_image);
//            textView = itemView.findViewById(R.id.best_title);
        }

        void setImage(String imageResource) {
//            imageView.setImageResource(context.getResources().getIdentifier(imageResource, "drawable", context.getPackageName()));
        }
//        void setText(String textTitle){
//            textView.setText(textTitle);
//        }
    }
}