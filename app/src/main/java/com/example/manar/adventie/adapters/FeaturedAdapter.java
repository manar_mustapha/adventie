package com.example.manar.adventie.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.example.manar.adventie.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by manar on 17/10/17.
 */

public class FeaturedAdapter extends RecyclerView.Adapter<FeaturedAdapter.BestViewHolder> {

    List<String> images = new ArrayList<>();
    Context context;

    public FeaturedAdapter(Context context) {

        this.context = context;
        images.add("featured_by_gray");
        images.add("featured_by_gray");
        images.add("featured_by_gray");
        images.add("featured_by_gray");
        images.add("featured_by_gray");
        images.add("featured_by_gray");
        images.add("featured_by_gray");
        images.add("featured_by_gray");
        images.add("featured_by_gray");
        images.add("featured_by_gray");
        images.add("featured_by_gray");
        images.add("featured_by_gray");
    }

    @Override
    public FeaturedAdapter.BestViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.featured_item, parent, false);
        return new FeaturedAdapter.BestViewHolder(view);
    }

    @Override
    public void onBindViewHolder(FeaturedAdapter.BestViewHolder holder, int position) {

        holder.setImage(images.get(position));
        holder.setText("PAUL");
        holder.setRate(4);
    }

    @Override
    public int getItemCount() {
        return images.size();
    }

     class BestViewHolder extends RecyclerView.ViewHolder {

         ImageView imageView;
         TextView textView;
         RatingBar ratingBar;

        BestViewHolder(View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.featured_image);
            textView = itemView.findViewById(R.id.featured_text);
            ratingBar = itemView.findViewById(R.id.rate);
        }

        void setImage(String imageResource) {
            imageView.setImageResource(context.getResources().getIdentifier(imageResource, "drawable", context.getPackageName()));
        }
        void setText(String textTitle){
            textView.setText(textTitle);
        }

        void setRate(int rate){
            ratingBar.setProgress(rate);
        }
    }
}