package com.example.manar.adventie.adapters;

import android.content.Context;
import android.content.Intent;
import android.provider.MediaStore;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.example.manar.adventie.R;

import java.util.ArrayList;
import java.util.List;
import java.util.zip.Inflater;

/**
 * Created by manar on 17/10/17.
 */

public class DontMissAdapter extends RecyclerView.Adapter<DontMissAdapter.DontMissViewHolder> {

    List<String> images = new ArrayList<>();
    Context context;

    public DontMissAdapter(Context context) {

        this.context = context;
        images.add("group_4");
        images.add("group_4");
        images.add("group_4");
        images.add("group_4");
        images.add("group_4");
        images.add("group_4");
        images.add("group_4");
        images.add("group_4");
        images.add("group_4");
        images.add("group_4");
        images.add("group_4");
        images.add("group_4");
    }

    @Override
    public DontMissViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.dont_miss_this_item, parent, false);
        return new DontMissViewHolder(view);
    }

    @Override
    public void onBindViewHolder(DontMissViewHolder holder, int position) {

        holder.setImage(images.get(position));
    }

    @Override
    public int getItemCount() {
        return images.size();
    }

    class DontMissViewHolder extends RecyclerView.ViewHolder {

        ImageView imageView;

        DontMissViewHolder(View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.dont_miss_item);
        }

        void setImage(String imageResource) {
            imageView.setImageResource(context.getResources().getIdentifier(imageResource, "drawable", context.getPackageName()));
        }
    }
}