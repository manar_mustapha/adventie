package com.example.manar.adventie.view;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import com.example.manar.adventie.R;
import com.example.manar.adventie.adapters.BrowseAllAdapter;

public class BrowseActivity extends AppCompatActivity {

    RecyclerView browseRecycleView;
    BrowseAllAdapter browseAllAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_browse);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setLogo(R.drawable.a_v_icn_back_white);
        browseRecycleView = findViewById(R.id.browse_recycle_view);
        browseRecycleView.setHasFixedSize(true);
        browseRecycleView.setLayoutManager(new LinearLayoutManager(getApplicationContext() , LinearLayoutManager.VERTICAL , false));

    }

    @Override
    protected void onStart() {
        super.onStart();
        browseAllAdapter = new BrowseAllAdapter(this);
        browseRecycleView.setAdapter(browseAllAdapter);
    }
}
